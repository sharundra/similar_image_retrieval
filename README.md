# Image Retrieval (via Autoencoders)

Given a set of query images and database images, we perform image retrieval on database images to get the top-k most similar database images using kNN on the image embeddings with cosine similarity as the distance metric. As an example, we provide 36 fashion images (6 of each women garment class) and perform similar image retrieval by querying 3 unseen test images.

We provide the unsupervised methods here: 


### **Training Autoencoders** 

We train both a simple autoencoder and a convolutional autoencoder on our database images with the objective of minimizing reconstruction loss. After sufficient training, we extract the encoder part of the autoencoder and use it during inference to generate flattened embeddings.


![no image display](coverart/AE_concept.jpg)



![no image display](coverart/convAE_reconstruct.png)


## Visualizations
 

### **Convolutional Autoencoder**


![no image display](coverart/convAE_retrieval_2.png)
![no image display](coverart/convAE_retrieval_1.png)
![no image display](coverart/convAE_retrieval_5.png)



 
![no image display](coverart/convAE_tsne.png)
 


![no image display](coverart/convAE_reconstruct.png)


### **Simple Autoencoder**


![no image display](coverart/simpleAE_retrieval_7.png)
![no image display](coverart/simpleAE_retrieval_6.png)
![no image display](coverart/simpleAE_retrieval_0.png)



![no image display](coverart/simpleAE_tsne.png)
 


![no image display](coverart/simpleAE_reconstruct.png)


## Usage

Run

```
python3 image_retrieval.py
```    

after selecting the model you want to use by editing the model name in `image_retrieval.py` 

```
modelName = "convAE"  # try: "simpleAE", "convAE"
trainModel = True
```

There are 2 models to choose from: 
* `"simpleAE"` = simple fully-connected autoencoder
* `"convAE"` = multi-layer convolutional autoencoder 

All output visualizations can be found in the `output` directory.

## Example output

```
Reading train images...
Reading test images...
Image shape = (100, 100, 3)
Loading VGG19 pre-trained model...
input_shape_model = (100, 100, 3)
output_shape_model = (3, 3, 512)
Applying image transformer to training images...
Applying image transformer to test images...
 -> X_train.shape = (36, 100, 100, 3)
 -> X_test.shape = (3, 100, 100, 3)
Inferencing embeddings using pre-trained model...
 -> E_train.shape = (36, 3, 3, 512)
 -> E_test.shape = (3, 3, 3, 512)
 -> E_train_flatten.shape = (36, 4608)
 -> E_test_flatten.shape = (3, 4608)
Fitting k-nearest-neighbour model on training images...
Performing image retrieval on test images...
Visualizing t-SNE on training images...
```

## Libraries

* tensorflow, skimage, sklearn, multiprocessing, numpy, matplotlib
